﻿using Chat.Tables;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Chat.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Rejestracja : ContentPage
	{
		public Rejestracja()
		{
			SetValue(NavigationPage.HasNavigationBarProperty, false);
			InitializeComponent();
		}

		private void Btn_rejestracja(object sender, EventArgs e)
		{
			var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "DataBase.db");
			var db = new SQLiteConnection(path);
			db.CreateTable<RejestracjaUzytkownika>();

			var item = new RejestracjaUzytkownika()
			{
				Imie = Imie.Text,
				Haslo = Haslo.Text,
				NumAlbumu = Numeralbumu.Text,
				Email = Email.Text,
				DataUrodzenia = DataUrodzenia.Text,
				Telefon = Telefon.Text

			};

			db.Insert(item);
			Device.BeginInvokeOnMainThread(async () =>
			{
				var result = await this.DisplayAlert("Gratulacje!", "Użytkownik jest zarejestrowany", "Tak", "Skasuj");
				if (result)
					await Navigation.PushAsync(new Logowanie());
			});
		}

		private void Button_Clicked(object sender, EventArgs e)
		{

		}

		async void Btn_wroc(object sender, EventArgs e)
		{
			
				await Navigation.PushAsync(new Logowanie());
			
		}
	}
}