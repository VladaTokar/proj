﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.Tables
{
	class RejestracjaUzytkownika
	{
		public Guid UzytkownikId { get; set; }
		public string Imie { get; set; }
		public string Haslo { get; set; }
		public string NumAlbumu { get; set; }
		public string Email { get; set; }
		public string DataUrodzenia { get; set; }
		public string Telefon { get; set; }

	}
}
